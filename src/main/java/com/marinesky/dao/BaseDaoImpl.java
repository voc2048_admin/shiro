package com.marinesky.dao;/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
/
                     Created by Marinesky on 2017/5/1.
 */

import com.marinesky.dao.constants.DBMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import javax.annotation.Resource;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public abstract class BaseDaoImpl<T extends Object> extends SqlSessionDaoSupport implements BaseDao<T> {


    private String namespace;


    public BaseDaoImpl() {
        Class clz = this.getClass();
        ParameterizedType type = (ParameterizedType) clz.getGenericSuperclass();
        Type[] types = type.getActualTypeArguments();

        namespace = ((Class<T>) types[0]).getName();

    }

    @Resource
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        super.setSqlSessionFactory(sqlSessionFactory);
    }

    /**
     * 获取mapper的nameSpace.mapperId
     * @param mapperId
     * @return
     */
    private String getSqlId(String mapperId){
        return namespace+"."+mapperId;
    }

    /**
     * 通过自定义条件获取记录
     *
     * @param param
     * @return
     */
    @Override
    public T get(Map param) {
        return getSqlSession().selectOne(getSqlId(DBMapper.GET.getMapperId()),param);
    }

    /**
     * 通过自定义条件和mapperId获取记录
     *
     * @param param
     * @param mapperId
     * @return
     */
    @Override
    public T get(Map param, String mapperId) {
        return getSqlSession().selectOne(getSqlId(mapperId),param);
    }

    /**
     * 通过自定义条件和mapperId获取对象
     *
     * @param param
     * @param mapperId
     * @return
     */
    @Override
    public Object queryForObject(Map param, String mapperId) {
        return getSqlSession().selectOne(getSqlId(mapperId),param);
    }

    /**
     * 通过自定义条件获取记录数
     *
     * @param param
     * @return
     */
    @Override
    public long count(Map param) {
        return getSqlSession().selectOne(getSqlId(DBMapper.COUNT.getMapperId()),param);
    }

    /**
     * 通过自定义条件获取记录集合
     *
     * @param param
     * @return
     */
    @Override
    public List<T> find(Map param) {
        return getSqlSession().selectList(getSqlId(DBMapper.FIND.getMapperId()), param);
    }

    /**
     * 通过自定义条件和mapperId获取记录集合
     *
     * @param param
     * @param mapperId
     * @return
     */
    @Override
    public List<T> find(Map param, String mapperId) {
        return getSqlSession().selectList(getSqlId(mapperId),param);
    }

    /**
     * 通过自定义条件和mapperId获取对象集合
     *
     * @param param
     * @param mapperId
     * @return
     */
    @Override
    public List<Object> queryForObjects(Map param, String mapperId) {
        return getSqlSession().selectList(getSqlId(mapperId),param);
    }

    /**
     * 插入保存数据
     *
     * @param t
     * @return
     */
    @Override
    public T insert(T t) {
        getSqlSession().insert(getSqlId(DBMapper.INSERT.getMapperId()),t);
        return  t;
    }

    /**
     * 自定义插入保存数据
     *
     * @param param
     * @param mapperId
     * @return
     */
    @Override
    public int insert(Map param, String mapperId) {
        return  getSqlSession().insert(getSqlId(DBMapper.INSERT.getMapperId()),param);

    }

    /**
     * 更新数据
     *
     * @param t
     * @return
     */
    @Override
    public T update(T t) {
        getSqlSession().update(getSqlId(DBMapper.UPDATE.getMapperId()),t);
        return  t;
    }

    /**
     * 自定义条件和mapperId更新数据
     *
     * @param param
     * @param mapperId
     * @return
     */
    @Override
    public int update(Map param, String mapperId) {
        return  getSqlSession().update(getSqlId(mapperId),param);
    }

    /**
     * 自定义条件删除记录
     *
     * @param param
     * @return
     */
    @Override
    public int delete(Map param) {
        return getSqlSession().delete(getSqlId(DBMapper.DELETE.getMapperId()),param);
    }
}
