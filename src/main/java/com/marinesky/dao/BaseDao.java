package com.marinesky.dao;/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
/
                     Created by Marinesky on 2017/4/29.
 */

import java.util.List;
import java.util.Map;

/**
 * 数据库常用的操作接口
 * @param <T>  --基于哪个实例
 */
public interface BaseDao<T extends Object> {


    /**
     * 通过自定义条件获取记录
     * @param param
     * @return
     */
    public T get(Map param);

    /**
     * 通过自定义条件和mapperId获取记录
     * @param param
     * @param mapperId
     * @return
     */
    public T get(Map param, String mapperId);

    /**
     * 通过自定义条件和mapperId获取对象
     * @param param
     * @param mapperId
     * @return
     */
    public Object queryForObject(Map param,String mapperId);

    /**
     * 通过自定义条件获取记录数
     * @param param
     * @return
     */
    public long count(Map param);

    /**
     * 通过自定义条件获取记录集合
     * @param param
     * @return
     */
    public List<T> find(Map param);

    /**
     * 通过自定义条件和mapperId获取记录集合
     * @param param
     * @param mapperId
     * @return
     */
    public List<T> find(Map param,String mapperId);

    /**
     * 通过自定义条件和mapperId获取对象集合
     * @param param
     * @param mapperId
     * @return
     */
    public List<Object> queryForObjects(Map param,String mapperId);

    /**
     * 插入保存数据
     * @param t
     * @return
     */
    public T insert(T t);

    /**
     * 自定义插入保存数据
     * @param param
     * @param mapperId
     * @return
     */
    public int insert(Map param,String mapperId);

    /**
     * 更新数据
     * @param t
     * @return
     */
    public T update(T t);


    /**
     * 自定义条件和mapperId更新数据
     * @param param
     * @param mapperId
     * @return
     */
    public int update(Map param,String mapperId);

    /**
     * 自定义条件删除记录
     * @param param
     * @return
     */
    public int delete(Map param);




}
