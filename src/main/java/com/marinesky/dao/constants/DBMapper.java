package com.marinesky.dao.constants;/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
/
                     Created by Marinesky on 2017/5/1.
 */

public enum DBMapper {
    GET("get","查询单条"),
    FIND("find","查询集合"),
    COUNT("count","查询记录数"),
    QUERY("query","查询对象"),
    INSERT("insert","插入数据"),
    UPDATE("update","更新数据"),
    DELETE("delete","删除数据");

    private String mapperId;

    private String mapperName;

    DBMapper(String mapperId, String mapperName) {
        this.mapperId = mapperId;
        this.mapperName = mapperName;
    }

    public String getMapperId() {
        return mapperId;
    }

    public String getMapperName() {
        return mapperName;
    }

    @Override
    public String toString() {
        return "DBMapper{" +
                "mapperId='" + mapperId + '\'' +
                ", mapperName='" + mapperName + '\'' +
                '}';
    }
}
