package com.marinesky.common.generator;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.FileResourceLoader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
/
                     Created by Marinesky on 2017/5/2.
 */
public class BeetlGenerator {

    private static BeetlGenerator instance;
    /**
     * java代码生成器
     *
     */
    private static final String DEFAULT_CODE = "UTF-8";

    /**
     * 模板对象
     */
    private static GroupTemplate gt;

    public static synchronized BeetlGenerator getInstance(){
        if (instance == null) {
            instance = new BeetlGenerator();
            FileResourceLoader fileResourceLoader = new FileResourceLoader(instance.getClass().getResource("/template").getPath(), DEFAULT_CODE);
            Configuration cfg = null;
            try {
                cfg = Configuration.defaultConfiguration();
                cfg.setCharset(DEFAULT_CODE);
                gt = new GroupTemplate(fileResourceLoader, cfg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }



    /**
     * 生成模板文件
     * @param params
     * @param templateName
     */
    public void generatorFile(GeneratorParams params,String templateName){
        try {
            File targetFile = new File(params.getJavaFileName());
            if(targetFile.exists()){
                targetFile.delete();
            }else{
                File parentDir = targetFile.getParentFile();
                if(!parentDir.exists()){
                    parentDir.mkdirs();
                }
            }
            Template template = gt.getTemplate(templateName);
            template.binding("generator",params);
            template.renderTo(new OutputStreamWriter(new FileOutputStream(targetFile),DEFAULT_CODE));

        }catch (Exception e){
            e.printStackTrace();
            System.err.println("错误[系统异常:"+e.getMessage()+"]");
        }

    }
}
