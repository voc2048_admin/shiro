package com.marinesky.common.mybatis.sql;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.ListUtilities;

/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
/
                     Created by Marinesky on 2017/4/28.
 */
public class SqlCountElementGenerator extends  AbstractElementGenerator  {

    /**
     * 自定义生成mybatis count的方法
     */

    public SqlCountElementGenerator(IntrospectedTable introspectedTable) {
        super(introspectedTable);
    }

    @Override
    public XmlElement getElements() {

        XmlElement answer = new XmlElement("select");

        String fqjt = introspectedTable.getExampleType();

        answer.addAttribute(new Attribute("id", "count"));
        answer.addAttribute(new Attribute("parameterType", "java.util.Map"));
        answer.addAttribute(new Attribute("resultType", "java.lang.Long"));

        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from ");
        sb.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
        answer.addElement(new TextElement(sb.toString()));

        XmlElement whereElement = getWhereElement(ListUtilities.removeGeneratedAlwaysColumns(introspectedTable.getAllColumns()));
        answer.addElement(whereElement);

        XmlElement orderByElement = new XmlElement("if");
        answer.addElement(orderByElement);
        sb.setLength(0);
        sb.append(ORDER_BY_CLAUSE);
        sb.append(" != null");
        orderByElement.addAttribute(new Attribute("test", sb.toString()));
        sb.setLength(0);

        sb.append(" ORDER BY  ${orderByClause} ");
        orderByElement.addElement(new TextElement(sb.toString()));

       return  answer;
    }
}
