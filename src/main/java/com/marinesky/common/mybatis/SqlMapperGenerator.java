package com.marinesky.common.mybatis;

import com.marinesky.common.mybatis.sql.*;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

import java.util.List;

/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
/
                     Created by Marinesky on 2017/4/28.
 */
public class SqlMapperGenerator extends PluginAdapter {
    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }


    public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {

        XmlElement parentElement = new XmlElement("mapper");
        parentElement.addAttribute(new Attribute("namespace",introspectedTable.getBaseRecordType()));
        document.setRootElement(parentElement);

        //BaseResultMap生成器
        ResultMapElementGenerator resultMapElementGenerator = new ResultMapElementGenerator(introspectedTable);

        //Base_Column_List生成器
        ColumnListElementGenerator columnListElementGenerator = new ColumnListElementGenerator(introspectedTable);

        //get mapper生成
        SqlGetElementGenerator sqlGetElementGenerator = new SqlGetElementGenerator(introspectedTable);
        //find mapper生成
        SqlFindElementGenerator sqlFindElementGenerator = new SqlFindElementGenerator(introspectedTable);
        //count mapper生成
        SqlCountElementGenerator sqlCountElementGenerator = new SqlCountElementGenerator(introspectedTable);

        //insert mapper生成
        SqlInsertElementGenerator sqlInsertElementGenerator = new SqlInsertElementGenerator(introspectedTable);

        //update mapper生成
        SqlUpdateElementGenerator sqlUpdateElementGenerator = new SqlUpdateElementGenerator(introspectedTable);


        //delete mapper生成
        SqlDeleteElementGenerator sqlDeleteElementGenerator = new SqlDeleteElementGenerator(introspectedTable);




        parentElement.addElement(new TextElement("<!-- 基础结果集 -->"));
        parentElement.addElement(resultMapElementGenerator.getElements());

        parentElement.addElement(new TextElement("<!-- 基础数据列 -->"));
        parentElement.addElement(columnListElementGenerator.getElements());

        parentElement.addElement(new TextElement("<!-- 查询获取记录(主键或唯一索引) -->"));
        parentElement.addElement(sqlGetElementGenerator.getElements());

        parentElement.addElement(new TextElement("<!-- 查询多条记录(支持分页和排序) -->"));
        parentElement.addElement(sqlFindElementGenerator.getElements());

        parentElement.addElement(new TextElement("<!-- 查询记录数(支持排序) -->"));
        parentElement.addElement(sqlCountElementGenerator.getElements());

        parentElement.addElement(new TextElement("<!-- 新增 -->"));
        parentElement.addElement(sqlInsertElementGenerator.getElements());

        parentElement.addElement(new TextElement("<!-- 修改 -->"));
        parentElement.addElement(sqlUpdateElementGenerator.getElements());


        parentElement.addElement(new TextElement("<!-- 删除 -->"));
        parentElement.addElement(sqlDeleteElementGenerator.getElements());

        return super.sqlMapDocumentGenerated(document, introspectedTable);
    }

}
