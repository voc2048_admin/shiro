package com.marinesky.coder;

import com.marinesky.common.generator.BeetlGenerator;
import com.marinesky.common.generator.GeneratorParams;
import org.junit.Test;

/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
/
                     Created by Marinesky on 2017/5/2.
 */
public class BettelGenerator {




    @Test
    public void testIService() throws Exception {

        String[] models = new String[]{"Organization","Resource","Role","RoleResource","User","UserRole"};
        for (int i=0;i<models.length;i++){
            String model= models[i];

            GeneratorParams iServiceParams = new GeneratorParams(model,"com.marinesky.service","D:\\service\\","I"+model+"Service.java");

            BeetlGenerator.getInstance().generatorFile(iServiceParams,"IService.bt");

            GeneratorParams serviceParams = new GeneratorParams(model,"com.marinesky.service","D:\\service\\impl",model+"Service.java");

            BeetlGenerator.getInstance().generatorFile(serviceParams,"Service.bt");

        }








    }
}
